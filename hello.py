from flask import Flask, render_template, session, redirect, url_for, flash
from flask.ext.script import Manager
from flask.ext.bootstrap import Bootstrap
from flask.ext.moment import Moment
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField
from wtforms.validators import Required

app = Flask(__name__)

# To implement CSRF protection, Flask-WTF needs the app to configure an
#encryption key. This key is used to generate a token to verify the
#authenticity of requests with form data. 

app.config['SECRET_KEY'] = 'hard to guess string' #-> should be in ENV not in code.

# The 'app.config' DICTIONARY is a General Purpose place to store 
#configuration variables used by the framework, the extension, or the
#application itself. 

manager = Manager(app)
bootstrap = Bootstrap(app)
moment = Moment(app)

# The "fields in the form"(name, submit) are defined as class variables.
#each class variable is assigned an object associated with the fieldtype.  
class NameForm(Form):
    name = StringField('What is your name?', validators=[Required()])
    submit = SubmitField('Submit')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500

# ROUTE: The association between a URL and the function that it handles. 
#   -The following view function implements 'redirects' and 
#'user sessions'
@app.route('/', methods=['GET', 'POST'])
def index():
    form = NameForm()
    if form.validate_on_submit():
        old_name = session.get('name')
        if old_name is not None and old_name != form.name.data:
            flash('Looks like you have changed your name!')
        session['name'] = form.name.data
        form.name.data = ''
        #The first and only required argument to 'url_for()' is the 
        #endpoint name, (i,e, the internal name each route has) by 
        #default, the endpoint of a route is the name of the view 
        #function attached to it. (I.e. 'def index()' = 'index' below)
        return redirect(url_for('index'))
    #If the session.get() fails, 'None' is returned. This is a standard
    #python dictionary feature for missing keys.
    return render_template('index.html', form=form, name=session.get('name'))


if __name__ == '__main__':
    manager.run()
